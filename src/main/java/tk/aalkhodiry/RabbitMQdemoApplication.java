package tk.aalkhodiry;

import com.rabbitmq.client.*;
import com.rabbitmq.http.client.Client;
import com.rabbitmq.http.client.domain.UserPermissions;
import org.kohsuke.randname.RandomNameGenerator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitManagementTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.TimeoutException;

@SpringBootApplication
public class RabbitMQdemoApplication implements CommandLineRunner {
    //    @Autowired
    //    private ConfigurableApplicationContext context;

    @Autowired
    private Client rabbitClient;

    private static final Logger LOG = LoggerFactory.getLogger(RabbitMQdemoApplication.class);

    @Autowired
    private RabbitManagementTemplate managementTemplate;

//    final static String queueName = "newtest";

//    @Bean
//    Queue queue(){
//        return new Queue(queueName, true);
//    }
//
//    @Bean
//    TopicExchange exchange(){
//        return new TopicExchange("newTxTest");
//    }

    //    @Bean
//    Binding binding(Queue queue, TopicExchange exchange){
//        return BindingBuilder.bind(queue).to(exchange).with(queueName);
//    }

//    @Bean
//    RestTemplate restTemplate() {
//        LOG.debug(String.format("creating RestTemplate. usename/password: %s, %s", restUsername, restPassword));
//        CredentialsProvider credProvider = new BasicCredentialsProvider();
//        credProvider.setCredentials(
//                new AuthScope(String.format("%s://%s", restProtocol, restHost), restPort),
//                new UsernamePasswordCredentials(restUsername, restPassword));
//        final CloseableHttpClient closeableHttpClient = HttpClients.custom().setDefaultCredentialsProvider(credProvider).build();
//        return new RestTemplate(new HttpComponentsClientHttpRequestFactory(closeableHttpClient));
//    }


//    @Bean
//    SimpleMessageListenerContainer container(ConnectionFactory connectionFactory, MessageListenerAdapter listenerAdapter) {
//        SimpleMessageListenerContainer container = new SimpleMessageListenerContainer();
//        container.setConnectionFactory(connectionFactory);
//        container.setQueueNames(queueName);
//        container.setMessageListener(listenerAdapter);
//        return container;
//    }

//    @Bean
//    Receiver receiver() {
//        return new Receiver();
//    }

//    @Bean
//    MessageListenerAdapter listenerAdapter(Receiver receiver) {
//        return new MessageListenerAdapter(receiver, "receiveMessage");
//    }


    public static void main(String[] args) {
        SpringApplication app = new SpringApplication(RabbitMQdemoApplication.class);
        app.setWebEnvironment(false);
        final ConfigurableApplicationContext ctx = app.run(args);
        LOG.debug(String.format("%s application started", ctx.getApplicationName()));
    }


//    private HttpHeaders getHeaders() {
//        final ArrayList<MediaType> types = new ArrayList<MediaType>();
//        types.add(MediaType.APPLICATION_JSON);
//
//        HttpHeaders headers = new HttpHeaders();
//        headers.setContentType(MediaType.APPLICATION_JSON);
//        headers.setAccept(types);
//        headers.set("Authorization", "Basic " + new String(Base64.encodeBase64((restUsername + ":" + restPassword).getBytes(Charset.forName("US-ASCII")))));
//
////        LOG.debug(String.format("Headers: %s", headers.toString()));
//        return headers;
//    }


    @Override
    public void run(String... args) throws Exception {
        receiveMessages();
    }

    private void receiveMessages() throws IOException, TimeoutException {
        RandomNameGenerator rnd = new RandomNameGenerator(0);
        final String vhostName, username;
        String password = "test";
        Connection conn;
        final Channel channel;
        int numberOfMessages = 500000;
        ConnectionFactory factory = new ConnectionFactory();
        factory.setConnectionTimeout(6 * 10000);
        factory.setHandshakeTimeout(6 * 10000);


        vhostName = "continental_carrot";
        username = "formal_designer";
        LOG.debug(String.format("connecting: %s, %s", vhostName, username));


        factory.setUsername(username);
        factory.setPassword(password);
        factory.setVirtualHost(vhostName);

        conn = factory.newConnection();
        channel = conn.createChannel();


        boolean autoAck = false;
        String queueName = "testQueue" + 0;

        channel.basicConsume(queueName, autoAck, "myConsumerTag",
                new DefaultConsumer(channel) {
                    @Override
                    public void handleDelivery(String consumerTag,
                                               Envelope envelope,
                                               AMQP.BasicProperties properties,
                                               byte[] body)
                            throws IOException {
                        String routingKey = envelope.getRoutingKey();
                        String contentType = properties.getContentType();
                        long deliveryTag = envelope.getDeliveryTag();
                        String message = new String(body, "UTF-8");;
//                        LOG.debug(String.format("routingKey: %s, contentType: %s, Message: %s", routingKey, contentType, message));
                        channel.basicAck(deliveryTag, false);
                    }
                });
    }

    private void createAndSendingMessages() throws InterruptedException {
        RandomNameGenerator rnd = new RandomNameGenerator(3);
        String vhostName, username;
        String password = "test";
        UserPermissions permission = new UserPermissions(".*", ".*", ".*");
        Connection conn;
        Channel channel;
        int numberOfMessages = 500000;
        String exchangeName, queueName, routingKey;
        byte[] messageBodyBytes = "{\"id\" = \"1\"}".getBytes();

        ConnectionFactory factory = new ConnectionFactory();
        factory.setConnectionTimeout(10 * 10000);
        factory.setHandshakeTimeout(10 * 10000);

        for (int q = 0; q < 3; q++) {
            final long startTime = System.nanoTime();
            try {
                vhostName = rnd.next();
                username = rnd.next();
                exchangeName = "testExchange" + q;
                queueName = "testQueue" + q;
                routingKey = "testRoutingKey" + q;

                LOG.debug(String.format("Creating a vhost: %s, username: %s", vhostName, username));
                factory.setUsername(username);
                factory.setPassword(password);
                factory.setVirtualHost(vhostName);
//                factory.setHost("localhost");
//                factory.setPort(15672);

                rabbitClient.createVhost(vhostName);
                rabbitClient.createUser(username, password.toCharArray(), new ArrayList<String>());
                rabbitClient.updatePermissions(vhostName, username, permission);

                LOG.debug(String.format("Connecting: %s", factory.getVirtualHost()));
                conn = factory.newConnection();
                channel = conn.createChannel();

                channel.exchangeDeclare(exchangeName, "direct", true);
                channel.queueDeclare(queueName, true, false, false, null);
                channel.queueBind(queueName, exchangeName, routingKey);

                for (int i = 0; i < numberOfMessages; i++) {
                    channel.basicPublish(exchangeName, routingKey, MessageProperties.PERSISTENT_TEXT_PLAIN, messageBodyBytes);
                }
                channel.close();
                conn.close();
            } catch (Exception e) {
                LOG.error("Failed...", e);
            }
            final long elapsedTime = System.nanoTime() - startTime;
            LOG.debug(String.format("Time needed to create and send %s messages to rabbitMQ: %s sec", numberOfMessages, (elapsedTime / 1000000000L)));
            Thread.sleep(2000);
        }
    }
}
