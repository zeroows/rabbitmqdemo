package tk.aalkhodiry.config;

import com.rabbitmq.http.client.Client;
import org.springframework.amqp.core.AmqpAdmin;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.amqp.rabbit.core.RabbitManagementTemplate;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;

@Configuration
public class RabbitConfiguration {

    @Value("${app.resttemplate.url}")
    private String rabbitUrl;

    @Value("${app.resttemplate.username}")
    private String rabbitUsername;

    @Value("${app.resttemplate.password}")
    private String rabbitPassword;

    @Bean
    public Client rabbitClient() throws MalformedURLException, URISyntaxException {
        return new Client(new URL(rabbitUrl), rabbitUsername, rabbitPassword);
    }

    @Bean
    public RabbitManagementTemplate rabbitManagementTemplate(Client rabbitClient) {
        return new RabbitManagementTemplate(rabbitClient);
    }
}